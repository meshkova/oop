package ru.mea.replaceAll.readers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import static java.nio.charset.StandardCharsets.*;

/**
 * Класс, позволяющий получить строку из файла
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class ReadFromFile implements IReader {


    @Override
    public String reader() {
        return null;

    }

    public static List<String> readere() throws IOException {
        Path path = Paths.get("D:\\Work\\tt.txt");

        return Files.readAllLines(path, UTF_8);
    }


}
