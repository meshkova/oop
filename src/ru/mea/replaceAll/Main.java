package ru.mea.replaceAll;

import ru.mea.replaceAll.printers.*;
import ru.mea.replaceAll.readers.Console;
import ru.mea.replaceAll.readers.IReader;
import ru.mea.replaceAll.readers.PredefinedReader;
import ru.mea.replaceAll.readers.ReadFromFile;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader("Привет:) Пока-пока:)");
        IPrinter printer = new ConsolePrinter();
        IPrinter advPrinter = new AdvConsolePrinter();
        IPrinter decorativeConsole = new DecorativeConsolePrinter();
        IReader consol = new Console();

        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, advPrinter);
        Replacer decorativeReplacer = new Replacer(reader, decorativeConsole);

        replacer.replace();
        advReplacer.replace();
        decorativeReplacer.replace();
        String str = consol.reader();
        System.out.println(str);
        try {
            System.out.println(ReadFromFile.readere());
        } catch (IOException e) {
            System.out.println("Файл не найден");
        }
        try {
            WriteFromFile.newWrite();
        } catch (IOException e) {
            System.out.println("Файл не найден");
        }

    }


}
