package ru.mea.replaceAll.printers;

public interface IPrinter {
    void print(final String text);
}
