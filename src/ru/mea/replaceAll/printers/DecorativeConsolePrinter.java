package ru.mea.replaceAll.printers;

/**
 * Класс, демонстрирующий консольный принтер
 * который печатает звёздочки
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class DecorativeConsolePrinter implements IPrinter {

    @Override
    public void print(final String text) {
        System.out.println("**" + "Звёздочки" + "**");
    }
}
