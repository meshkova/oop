package ru.mea.replaceAll.printers;

/**
 * Класс, выводящий в консоль строку и длину строки
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class AdvConsolePrinter implements IPrinter {

   public void print(final String text) {
        System.out.println(text);
        int length = text.length();
        System.out.println(length);
    }
}