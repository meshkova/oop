package ru.mea.replaceAll;

import ru.mea.replaceAll.printers.IPrinter;
import ru.mea.replaceAll.readers.IReader;

class Replacer {


    private IReader reader;
    private IPrinter printer;

    public Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace() {
        final String text = reader.reader();
        final String replacedText = text.replaceAll(":\\)", ":-)");
        printer.print(replacedText);
    }
}
