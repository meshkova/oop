package ru.mea.identifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Класс, демонстрирующий поиск идентификатора
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Identifier {
    public static void main(String[] args) {
        System.out.println("Ввыводим текст кода в консоль");
        String result = "";
        try {
            System.out.println(readere());
            result = readere();
        } catch (IOException e) {
            System.out.println("Файл не найден");
        }

        System.out.println("\n" + "Ищем все идентификаторы");
        System.out.println(regular(result));

        System.out.println("\n" + "Выводим идентификаторы без повторений");
        System.out.println(regular1( result));
    }

    /**
     * Метод, указывающий путь к произвольной программе, на языке java
     *
     * @return список
     * @throws IOException исключения
     */
    static String readere() throws IOException {
        Path path = Paths.get("D:\\Work\\OOP\\src\\ru\\mea\\books\\Book.java");
        List<String> list = Files.readAllLines(path, UTF_8);

        return String.valueOf(list);
    }

    /**
     * Метод, ищущий идентификаторы по регулярному выражению
     *
     * @param result результат работы метода readere()
     * @return строку
     */
    static String regular(String result) {
        Pattern pattern = Pattern.compile("([A-Za-z$_](\\w*))");
        Matcher matcher = pattern.matcher(result);
        String answer = "";
        while (matcher.find()) {
            answer += matcher.group();
        }
        return answer;
    }

    /**
     * Метод, возвращающий уникальные идентификаторы
     *
     * @param result результат работы метода readere()
     * @return список
     */
    private static String regular1( String result) {
        Pattern pattern = Pattern.compile("([A-Za-z$_](\\w*))");
        Matcher matcher = pattern.matcher(result);
        HashSet<String> answer = new HashSet<>();
        while (matcher.find()) {
            answer.add(matcher.group());
        }
        return answer.toString();
    }
}



