package ru.mea.regular;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *Класс, демонстрирующий поиск  вещественных чисел
 * по регулярным выражениям
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Regular {
    public static void main(String[] args) {
        String string = "2.5 -5.78 плюс + +67 .8 9. +.  6.34e-10 -7.8e+10 23.10E+10";
        Pattern pattern = Pattern.compile("([+-]?(\\d*\\.\\d+)|(\\d+\\.\\d*))\\W");
        Pattern pattern1 = Pattern.compile("([+-]?(\\d*\\.\\d+[Ee][+-]?[\\d+])|(\\d+\\.\\d*[Ee][+-]?[\\d+]))");

        System.out.println("Ввывод дробных чисел с фиксированной точкой:" + regular(string, pattern));
        System.out.println("Ввывод дробных чисел с плавающей точкой:" + regular(string, pattern1));
    }

    /**
     * Метод, реализующий поиск вещественных чисел
     *
     * @param string строка
     * @param pattern1 регулярное выражение
     * @return строку
     */
    private static String regular(String string, Pattern pattern1) {
        Matcher matcher = pattern1.matcher(string);
        String answer = "";
        while (matcher.find()) {
            answer += matcher.group();
        }
        return answer;
    }
}