package ru.mea.obfuscator2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Класс, демонстрирующий работу мини-обфускатора 2 версия
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class FileObfuscator {
    private static String path;
    private static String newPath;

    public FileObfuscator(String path, String newPath) {
        this.path = path;
        this.newPath = newPath;
    }

    /**
     * Метод, получающий путь из файла
     *
     * @return путь
     */
    private static Path path() {
        Path strartPath = Paths.get(path); // D:\\Work\\OOP\\src\\ru\\mea\\quickSort\\QuickSort.java
        return strartPath;
    }


    /**
     * Метод, считывающий текст из файла
     *
     * @return строка
     * @throws IOException исключение
     */
    private static String reading(Path path) throws IOException {
        List<String> list;
        list = Files.readAllLines(path, UTF_8);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            builder.append(list.get(i));
        }
        return builder.toString();
    }

    /**
     * Метод, удаляющий комментарии
     *
     * @param string строка
     * @return строку без комментариев
     */
    private static String deletingComments(String string) {
        string = string.replaceAll("(\\/\\*([^*]|[\\r\\n]|(\\*+([^*\\/]|[\\r\\n])))*\\*+\\/)", ""); // для многострочного
        string = string.replaceAll("\\/\\/(([А-Яа-я\\w]+\\s{2,})|([А-Яа-я\\s\\w]+))\\s{2,}", ""); // Для однострочного комментария
        return string;
    }

    /**
     * Метод, удаляющий более 1-го пробела
     *
     * @param string строка без комментариев
     * @return строку без лишних пробелов
     */
    private static String removingSpaces(String string) {
        string = string.replaceAll("(\\s{2,})", "");
        return string;
    }

    /**
     * Медом, возвращающий имя файла  расширением
     *
     * @param path путь
     * @return имя с расширением
     */
    private static String gettingAname(Path path) {
        Path nameFile = path.getFileName();
        return nameFile.toString();

    }

    /**
     * Метод, удаляющий расширение файла
     *
     * @param strName строка содержащая имя файла с расширением
     * @return имя файла
     */
    private static String nameClass(String strName) {
        strName = strName.replaceAll("\\.\\w*", "");
        return strName;
    }

    /**
     * Метод, меняющий имя в файле
     *
     * @param str       строка содержащая путь к исходному файлу
     * @param nameClass строка содержащая имя файла без расширения
     * @return путь к файлу с новым именем
     */
    private static Path newPath(String str, String nameClass) {
        str = str.replaceAll(nameClass, "New" + nameClass);
        Path newPath = Paths.get(str);
        return newPath;
    }


    /**
     * Метод, меняюший имя класса и конструктора если такой имеется
     *
     * @param string строка
     * @return строку
     */
    private static String rename(String string, String nameClass) {
        string = string.replaceAll(nameClass, "New" + nameClass);
        return string;
    }


    /**
     * Метод, осуществляющий запись в новый файл
     *
     * @param string строка
     * @throws IOException исключение
     */
    private static void newWrite(String string, Path newPath) throws IOException {
        List<String> lines = Arrays.asList(string);
        Files.write(newPath, lines, Charset.forName("UTF-8")).toString();

    }

    public void obfuscate() {
        Path path = path();
        String str = String.valueOf(path);
        System.out.println(path);

        String string = "";

        try {
            string = reading(path);
            System.out.println(string);
        } catch (IOException e) {
            System.out.println("Файл не найден");
        }

        string = deletingComments(string);
        System.out.println(string);

        string = removingSpaces(string);
        System.out.println(string);

        String strName = gettingAname(path);
        System.out.println(strName);

        String nameClass = nameClass(strName);
        System.out.println(nameClass);

        Path newPath = newPath(str, nameClass);
        System.out.println(newPath);

        string = rename(string, nameClass);
        System.out.println(string);

        try {
            newWrite(string, newPath);
        } catch (IOException e) {
            System.out.println("Error");
        }


    }

    @Override
    public String toString() {
        return "Obfuscator{" +
                "path=" + path +
                ", newPath=" + newPath +
                '}';
    }
}

