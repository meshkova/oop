package ru.mea.obfuscator2;


import java.util.Scanner;


public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите путь к исходному файлу");
        String path = scanner.nextLine();   // D:\\Work\\OOP\\src\\ru\\mea\\regular\\Regular.java
        System.out.println("Введите путь к новому файлу");
        String newPath = scanner.nextLine();   // D:\\Work\\OOP\\src\\ru\\mea\\regular\\NewRegular.java
        FileObfuscator obfuscator = new FileObfuscator(path, newPath);
        obfuscator.obfuscate();
    }

}
