package ru.mea.books;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс, демонстрирующий электронную библиотеку
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Book fathersAndSons = new Book("Тургеньев", "Отцы и дети", 1883, Genre.NOVEL, "D:\\Work\\OOP\\src\\ru\\mea\\books\\fathersAndSons.doc");
        Book Potter = new Book("Роулинг Джоан", "Гарри Поттер", 2007, Genre.HAIRDRYER, "D:\\Work\\OOP\\src\\ru\\mea\\books\\Potter.doc");
        Book scarletFlower = new Book("Аксаков", "Аленький цветочек", 1858, Genre.FAIRY_TALES, "D:\\Work\\OOP\\src\\ru\\mea\\books\\scarletFlower.doc");
        Book it = new Book("Стивен Кинг", "Оно", 2015, Genre.HORRORS, "D:\\Work\\OOP\\src\\ru\\mea\\books\\it.doc");
        Book historyOfTime = new Book("Стивен Хокинг", "История времени", 2017, Genre.SCIENCE, "D:\\Work\\OOP\\src\\ru\\mea\\books\\historyOfTime.doc");
        Book dracula = new Book("Брэм Стокер", "Дракула", 1897, Genre.HAIRDRYER, "D:\\Work\\OOP\\src\\ru\\mea\\books\\dracula.doc");
        Book my_my = new Book("Тургеньев", "Му-му", 1852, Genre.FAIRY_TALES, "D:\\Work\\OOP\\src\\ru\\mea\\books\\dracula.doc");
        ArrayList<Book> books = new ArrayList<>(Arrays.asList(fathersAndSons, Potter, scarletFlower, it, historyOfTime, dracula, my_my));
        ArrayList<Book> arrayFavoriteGenres = new ArrayList<>();
        ArrayList<Book> arrayFavoriteAuthor = new ArrayList<>();
        ArrayList<Book> book_title = new ArrayList<>();
        System.out.println("Сортировка по алфавиту: ");
        sort(books);
        print(books);
        System.out.println("Сортировка по убыванию: ");
        sortYear(books);
        print(books);
        System.out.println("Введите жанр");
        String genre = scanner.nextLine();
        System.out.println(genre(books, arrayFavoriteGenres, genre));
        System.out.println("Введите автора");
        String favorite_author = scanner.nextLine();
        System.out.println(author(books, arrayFavoriteAuthor, favorite_author));
        System.out.println("Введите название книги");
        String title = scanner.nextLine();
        title(books, title, book_title);
        printName(book_title);
    }

    /**
     * Сортировка пузырьком по алфавиту по фамилиям авторов
     *
     * @param array список книг
     */
    private static void sort(ArrayList<Book> array) {
        for (int i = 0; i < array.size(); i++) {
            boolean tmp = false;
            for (int j = 0; j < array.size() - i - 1; j++) {
                if (array.get(j).getAuthor().compareTo(array.get(j + 1).getAuthor()) > 0) {

                    tmp = true;
                    Book temp = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, temp);
                }
            }
            if (!tmp) {
                break;
            }

        }

    }

    /**
     * Сортировка по убыванию
     *
     * @param array список книг
     */
    private static void sortYear(ArrayList<Book> array) {
        for (int i = 0; i < array.size(); i++) {
            boolean tmp = false;
            for (int j = 0; j < array.size() - 1; j++) {
                if (array.get(j).getYear() < array.get(j + 1).getYear()) {
                    tmp = true;
                    Book temp = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, temp);
                }
            }
            if (!tmp) {
                break;
            }

        }

    }


    /**
     * Фильтрация по жанрам
     *
     * @param array               список всех книг
     * @param arrayFavoriteGenres список книг любимого жанра
     * @param genre               введённый жанр
     * @return отфильтрованный список
     */
    private static ArrayList<Book> genre(ArrayList<Book> array, ArrayList<Book> arrayFavoriteGenres, String genre) {

        for (Book book : array) {
            if (genre.equalsIgnoreCase(book.getGenre().getFavoriteGenre())) {
                arrayFavoriteGenres.add(book);
            }


        }
        return arrayFavoriteGenres;
    }

    /**
     * @param array               исходный список книг
     * @param arrayFavoriteAuthor список книг любимых авторов
     * @param favorite_author     Любимый автор
     * @return отфильтрованный список
     */
    private static ArrayList<Book> author(ArrayList<Book> array, ArrayList<Book> arrayFavoriteAuthor, String favorite_author) {
        for (Book book : array) {
            if (favorite_author.equalsIgnoreCase(book.getAuthor())) {
                arrayFavoriteAuthor.add(book);
            }
        }
        return arrayFavoriteAuthor;
    }


    /**
     * Поиск книг по названию и добавление их в список
     *
     * @param array      исходный список книг
     * @param title      название книги
     * @param book_title список любимых книг
     */
    private static void title(ArrayList<Book> array, String title, ArrayList<Book> book_title) {
        int i = 0;
        for (Book name : array) {
            if (title.equalsIgnoreCase(name.getTitle())) {
                book_title.add(name);
                i++;
            }
        }
        if (i == 0) System.out.println("Таких названий нет");

    }

    /**
     * @param array отсортированный список
     */
    private static void print(ArrayList<Book> array) {
        for (Book value : array) System.out.print(value + "\n");

    }

    /**
     * @param book_title список книг, названия которых введены с клавиатуры
     */
    private static void printName(ArrayList<Book> book_title) {
        System.out.println(Arrays.toString(new ArrayList[]{book_title}));

    }
}



