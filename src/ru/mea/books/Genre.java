package ru.mea.books;

public enum Genre {
    HAIRDRYER("фантастика"),
    HORRORS("ужасы"),
    FAIRY_TALES("сказки"),
    SCIENCE("наука"),
    NOVEL("роман");

    private String favoriteGenre;

    Genre(String genre) {
        this.favoriteGenre = genre;
    }

    public String getFavoriteGenre() {
        return favoriteGenre;

    }
}
