package ru.mea.books;

public class Book {
    // поля
    private String author;
    private String title;
    private int year;
    private Genre genre;
    private String way;
/*
Это многострочный комментарий
 */
    Book(String author, String title, int year, Genre genre, String way) {
        this.author = author;
        this.title = title;
        this.year = year;
        this.genre = genre;
        this.way = way;
    }

    /**
     * Геттер
     * Докумнтационный комментарий
     * @return автора
     */
    String getAuthor() {
        return author;
    }

    String getTitle() {
        return title;
    }

    int getYear() {
        return year;
    }

    Genre getGenre() {
        return genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", genre=" + genre +
                ", way='" + way + '\'' +
                '}';
    }
}
