package ru.mea.quickSort;

import java.util.Arrays;

/**
 * Класс, демонстрирующий быструю сортировку
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class QuickSort {
    public static void main(String[] args) {
        int[] array = {7, 9, 1, -5, 100};
        int[] arr = {2, 5, 10, -4};
        int left = 0;
        int right = arr.length - 1;
        System.out.println("Сортировка по убыванию");
        partition(arr, left, right);
        sorting(arr, left, right);
        System.out.println(Arrays.toString(arr));
        int low = 0;
        int high = arr.length - 1;
        System.out.println("Сортировка по возрастанию");
        quickSort(array, low, high);
        System.out.println(Arrays.toString(array));
    }

    /**Сортировка по убыванию
     *
     * @param arr массив
     * @param left начало
     * @param right конец
     * @return индекс элмента
     */
    private static int partition(int[] arr, int left, int right) {
        int pivot = arr[left];
        int i = left;
        for (int j = left + 1; j <= right; j++) {
            if (arr[j] > pivot) { // комментарий com
                i = i + 1;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i];
        arr[i] = arr[left];
        arr[left] = temp;

        return i;

    }

    private static void sorting(int[] arr, int left, int right) {
        if (left < right) {
            int q = partition(arr, left, right);
            sorting(arr, left, q);
            sorting(arr, q + 1, right);
        }
    }

    /**
     * Быстрая сортировка по возрастанию
     *
     * @param array массив
     * @param low   начало
     * @param high  конец
     */
    private static void quickSort(int[] array, int low, int high) {
        if (array.length == 0) return;
        if (low >= high) return;
        int middle = low + (high - low) / 2;
        int opora = array[middle];
        int i = low, j = high;
        while (i <= j) {
            while
            (array[i] < opora) {
                i++;
            }
            while (array[j] > opora) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            quickSort(array, low, j);
        if (high > i)
            quickSort(array, i, high);
    }
}

